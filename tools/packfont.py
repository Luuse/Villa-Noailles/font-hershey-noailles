# /usr/bin/env python2

import fontforge
import glob
import sys
import os
import shutil

font = sys.argv[1]

SFD_DIR = glob.glob('FINAL/'+font+'.sfd')
Formats = ['otf', 'eot', 'ttf', 'svg', 'woff', 'sfd', 'ufo']


for sfd in SFD_DIR:
    FontName = sfd.split("/")[-1].replace(".sfd", "")
    nameSplit = FontName.split('-') 
    print(nameSplit[len(nameSplit) - 1 ])
    FamilyName = nameSplit[0] + '-' + nameSplit[1] + '-' + nameSplit[2]
    dirExport = 'output/' + FontName
    os.mkdir(dirExport)
    font = fontforge.open(sfd)

    font.em = 1100
    font.ascent = 800
    font.descent = 300
    font.familyname = FamilyName 
    font.fontname = FontName
    font.fullname = FontName
    font.weight = nameSplit[len(nameSplit) - 1]
    font.copyright = "by Luuse (contact@luuse.io), SIL Open Font License"
    for form in Formats:
        font.generate(dirExport+'/'+FontName+'.'+form)
        print(form)

    print(sfd)
    print(FontName)
