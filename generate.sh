#!/usr/bin/env bash

font=$1
echo $font
mkdir output-svg/$font/over
cp output-svg/$font/*.svg output-svg/$font/over/

for gly in output-svg/$font/over/*.svg;
do
  file=`basename $gly .svg`

  inkscape --verb=EditSelectAllInAllLayers \
          --verb=SelectionUnGroup \
          --verb=ObjectToPath \
          --verb=StrokeToPath \
          --verb=SelectionUnion \
          --verb=SelectionReverse \
          --verb=FileSave \
          --verb=FileClose \
          --verb=FileQuit \
					$gly \

	echo $file

  # inkscape $gly --export-ps="simple/$file.ps"
  # # inkscape "simple/$file.eps" --export-plain-svg="simple/$file.svg"
  # # rm simple/$file.esp
done


