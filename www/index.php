<?php 
  header("Access-Control-Allow-Origin: *");
  include 'functions/functions.php';
  include 'functions/Parsedown.php';
?>

<!DOCTYPE html>
<html>
  <head>
    <title>Hershey Noailles</title>
    <meta charset="UTF-8">
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-3.2.1.min.js"></script>
  </head>
  <body>
    <header>
      <?php
        $lines = file("README.md");
        $MD = new Parsedown();
        foreach($lines as $n => $line){
          echo $MD->text($line);
        }
      ?>
    </header>
    <div class="content">
      <div class="list">
        <div class="search-bar">
          <input type="text" name="search" value="" autocomplete="off" id="myinput" onkeyup="searchFunction()" placeholder="Search">
        </div>
          <?php
            ScanDirectory('../output');
          ?>
      </div>
      <div class="cont-fiche">

      </div>
    </div>
    <footer>
      <div class="log">
        <?php
          $log = shell_exec("git log --pretty=format:'<span class=\"date\" >%ad</span><span class=\"commit\" >|-> %s%d [%an]</span>' --graph --date=short");
        ?>
        <pre background="transparent">
        <?= $log; ?>
        </pre>
      </div>
    </footer>
  </body>
  <script charset="utf-8">
        
    function loadFiche(){
      $('.list ul').click(function(){
        var dirFont = $(this).find('span').text();
        console.log(dirFont);
        $('.cont-fiche').load('fiche.php?font='+dirFont);
        window.location.hash = dirFont;
      })
    }

    function searchFunction() {
      var input, filter, ul, li, a, i;
      input = document.getElementById('myinput');
      filter = input.value.toUpperCase();
      console.log(filter);
      list = document.getElementsByClassName('list');
      console.log(list);
      ul = document.getElementsByTagName('ul');

      for(i=0 ; i< ul.length; i++){
          a = ul[i].getElementsByTagName('span')[0];
          console.log(a);
          if(a.innerHTML.toUpperCase().indexOf(filter) > -1){
              ul[i].style.display = "";
          }else{
              ul[i].style.display = 'none';
          }
      }
    }

    $(document).ready(function(){
      loadFiche();
      var hashPost = window.location.hash.substring(1);
      if (hashPost && hashPost !== "") {
        $('.cont-fiche').load('fiche.php?font='+hashPost);
      }
      $('header, footer').click(function(){
        $(this).toggleClass('activate');
      })

    });
  </script>
</html>
