#!/usr/bin/env bash


font=$1
echo $font

for gly in output-svg/$font/*.svg;
do
  file=`basename $gly .svg`

  inkscape --verb EditSelectAllInAllLayers \
          --verb ObjectToPath \
          --verb SelectionCombine \
          --verb io.luuse.filter.hersheyrize-your-life-auto \
          --verb FileSave \
          --verb FileClose \
          --verb FileQuit \
  $gly \

done



