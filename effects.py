# /usr/bin/env python2

import glob
import sys
import os
# import json
import lxml.etree as ET

folderName = sys.argv[1]
dirFiles = 'output-svg/' + folderName + '/*.svg'
effectName = sys.argv[2]
outputName = folderName + '-' + effectName
cheminOut = 'output-svg/' + outputName

os.mkdir(cheminOut, 0o755)


for files in glob.glob(dirFiles):
    with open(files, 'rt') as f:
        treeLet = ET.parse(f)
    rootLet = treeLet.getroot()
    letWidth = rootLet.get('width')
    letDec = rootLet.get('data-dec')

    for pathLet in rootLet.iter():
        path_d = pathLet.attrib.get('d')
        if path_d:
            pathSvg = path_d

    with open('lib_effects/'+ effectName +'.svg', 'rt') as f:
        tree = ET.parse(f)

    root = tree.getroot()
    letWidth = rootLet.get('width')
    for path in root.iter():
        path_d = path.attrib.get('d')
        path_w = path.attrib.get('width')
        path_dec = path.attrib.get('data-dec')
        if path_d:
            path.set('d', str(pathSvg))
            path.set('{http://www.inkscape.org/namespaces/inkscape}original-d', str(pathSvg))
        if path_w:
            path.set('width', letWidth)
            path.set('data-dec', letDec)

    # print(root)
    tree.write( cheminOut + '/' + str(letDec) + '.svg')
    chemFile = cheminOut + '/' + str(letDec)
    # os.system('inkscape --file=' + chemFile + '.svg --export-eps='+chemFile+'.eps')
    # os.system('inkscape --file=' + chemFile + '.eps --export-plain-svg='+chemFile+'.svg')
