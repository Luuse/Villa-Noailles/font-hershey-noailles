clean-path:
	@read -p "Enter .svg folder : " folder; \
	python tools/clean-path.py $$folder 

font:
	@read -p "Enter .svg folder : " folder; \
	@read -p "Enter le déplacement des glyphs en Y: " y; \
	python tools/import.py $$folder 

svg2json:
	@read -p "Enter .svg folder : " folder; \
	python tools/svg2json.py $$folder 

stroke2path:
	@read -p "Enter font name : " font; \
	bash generate.sh $$font

packfont:
	@read -p "Enter font name : " font; \
	rm -rf output/$$font; \
	python tools/packfont.py $$font 

effect:
	@read -p "Enter .svg folder : " folder; 
	@read -p "Enter effect name : " eff; \
	python tools/effects.py ${folder} ${eff}

ungroup:
	@read -p "Enter .svg folder : " folder; \ 
	bash ungroup.sh $$folder
