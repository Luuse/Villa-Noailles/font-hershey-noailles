## Hershey Noailles
Luuse 2017

![Hershey Noailles](specimen/NoailleHershey1.png)
![Hershey Noailles](specimen/NoailleHershey2.png)

Petit programme pour faire des transformations de stroke de la Hershey Font
afin de pouvoir produire les fichiers fontes exécutable (otf, ttf, sfd). 

Ceci est un fork du projet [hersheytextjs](https://github.com/techninja/hersheytextjs) de Techninja.

![Hershey Noailles](specimen/interface.png)

### Requirements
  
  * python3
    * python-fontforge
    * math
    * lxml
    * elementTree
    * psMat
  * inkscape
  * apache

### Générer Svg

#### StrokeToPath
`$ bash generate.sh nom_folder_svg`

#### Générer la fonte
`$ python import.py nom_folder_svg`
Ensuite il faut ouvrir le `.otf `généré dans `FINAL/` puis l'enregistrer en
`.sfd` avec fontforge.

#### Générer les webfonts via le .sfd
Exemple: si le fichier est `FINAL/hershey-condensed.sfd`, 
la commande sera -> `$ python packfont.py hershey-condensed`
Le tout sera généré dans le dossier `output/`.

### Appliquer des effets de chemin
Dans ´lib_effects/´ mettre un svg avec l'effet.

Pour générer les svg d'une fonte avec cet effet il suffit de faire:
´python effects.py nomDuDossier nomEffet´

### Licence
JSON data Public Domain, All other code MIT Licensed.





